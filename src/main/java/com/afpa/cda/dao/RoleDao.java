package com.afpa.cda.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.afpa.cda.entity.Role;

@Repository
public interface RoleDao extends JpaRepository<Role, Integer> {
	public List<Role> findAll();

	public Optional<Role> findByName(String roleName);
}
