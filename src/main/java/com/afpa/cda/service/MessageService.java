package com.afpa.cda.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.MessageDao;
import com.afpa.cda.dao.UserDao;
import com.afpa.cda.dto.MessageDto;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.Message;
import com.afpa.cda.entity.User;

@Service
public class MessageService implements IMessageService {
	
	@Autowired
	MessageDao mDao;
	
	@Autowired
	UserDao uDao;
	private MessageDao messageDao;

	@Override
	public MessageDto add(MessageDto msg) {

		User user = uDao.findById(msg.getUserDto().getId()).get();
		
		Message m = mDao.save(Message.builder()
				.dateCreation(msg.getDateCreation())
				.user(user)
				.value(msg.getValue())
				.build()
				);
		
		return MessageDto.builder()
				.dateCreation(m.getDateCreation())
				.id(m.getId())
				.userDto(UserDto.builder()
						.id(user.getId())
						.name(user.getName())
						.lastCo(user.getLastCo())
						.build())
				.value(m.getValue())
				.build();
	}

	@Override
	public List<MessageDto> findAll() {
		return mDao.findAll().stream().map(x -> MessageDto.builder()
				.dateCreation(x.getDateCreation())
				.id(x.getId())
				.userDto(UserDto.builder()
						.id(x.getUser().getId())
						.name(x.getUser().getName())
						.lastCo(x.getUser().getLastCo())
						.build())
				.value(x.getValue())
				.build()).collect(Collectors.toList());
	}

	@Override
	public String deleteById(int id) {
		if (this.messageDao.existsById(id)) {
			try {
				this.messageDao.deleteById(id);
				return "OK";
			} catch (Exception e) {
				return "KO_SQL_EXCEPTION";
			}
		}
		return "KO";
	}
	
	

}
