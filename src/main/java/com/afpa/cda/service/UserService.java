package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.cda.dao.UserDao;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.User;

@Service
public class UserService implements IUserService {

	@Autowired
	UserDao uDao;

	public Optional<User> findByName(String name) {
		return uDao.findByName(name);
	}

	@Override
	public List<UserDto> findAll() {
		
		return uDao.findAll().stream().map(u -> UserDto.builder()
				.id(u.getId())
				.name(u.getName())
				.lastCo(u.getLastCo())
				.active(u.getActive())
				.build()).collect(Collectors.toList());
	}
}
