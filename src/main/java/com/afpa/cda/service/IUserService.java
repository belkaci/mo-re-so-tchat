package com.afpa.cda.service;

import java.util.List;
import java.util.Optional;

import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.User;

public interface IUserService {

	Optional<User> findByName(String name);

	List<UserDto> findAll();

}
