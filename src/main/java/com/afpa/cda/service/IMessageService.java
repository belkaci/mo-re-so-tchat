package com.afpa.cda.service;

import java.util.List;

import com.afpa.cda.dto.MessageDto;

public interface IMessageService {

	MessageDto add(MessageDto msg);

	List<MessageDto> findAll();

	String deleteById(int id);

}
