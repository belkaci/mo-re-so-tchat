package com.afpa.cda.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.afpa.cda.dto.UserDto;
import com.afpa.cda.service.RoleService;
import com.afpa.cda.service.UserService;

@Controller
@RequestMapping("user")
public class UserAdminController {
	@Autowired
	private UserService uServ;
	
	@Autowired
	private RoleService rServ;
	
	
	@GetMapping(path ="/users")
	public List<UserDto> listUsers(){
		return this.uServ.findAll();
	}
	
	
	
	
}
