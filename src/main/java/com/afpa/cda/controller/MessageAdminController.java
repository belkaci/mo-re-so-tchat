package com.afpa.cda.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.cda.dto.MessageDto;
import com.afpa.cda.dto.ReponseStatut;
import com.afpa.cda.dto.UserDto;
import com.afpa.cda.entity.User;
import com.afpa.cda.service.IMessageService;
import com.afpa.cda.service.IUserService;

@RestController
public class MessageAdminController {

	@Autowired
	private IMessageService mServ;
	
	@Autowired
	private IUserService uServ;
	
	@Autowired
	private IMessageService messageService;

	@GetMapping(path ="/messages")
	public List<MessageDto> list(){
		return this.mServ.findAll();
	}
	
	@PostMapping(path = "/messages")
	public MessageDto add(@RequestBody MessageDto msg, Authentication auth,HttpServletResponse res) {
		System.err.println(msg);
		if(auth == null) {
			res.setStatus(HttpStatus.FORBIDDEN.value());
			return null;
		}
		
		User user = uServ.findByName(auth.getName()).get();
		MessageDto mess = MessageDto.builder()
				.dateCreation(new Date())
				.userDto(UserDto.builder()
						.id(user.getId())
						.name(user.getName())
						.active(user.getActive())
						.build())
				.value(msg.getValue())
				.build();
		
		return this.mServ.add(mess);
	}
	
	@GetMapping(value = "/messages/{id}")
	public MessageDto recupererUnMessage(@PathVariable(value = "id") Integer id, HttpServletResponse resp) {
		Optional<MessageDto> findFirst = this.messageService.findAll().stream().filter(m->m.getId()==id).findFirst();
		if(findFirst.isPresent()) {
			return findFirst.get();
		}else {
			resp.setStatus(HttpStatus.NOT_FOUND.value());
			return null;
		}
	}
}
