package com.afpa.cda;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.afpa.cda.dao.UserDao;
import com.afpa.cda.entity.User;


public class SilmpleAuthHandler implements AuthenticationSuccessHandler {

	@Autowired
	UserDao userDao;
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication auth) throws IOException, ServletException {
		
		User u = userDao.findByName(auth.getName()).get();
		userDao.save(
				User.builder()
				.id(u.getId())
				.name(u.getName())
				.password(u.getPassword())
				.role(u.getRole())
				.creation(u.getCreation())
				.active(true)
				.lastCo(new Date())
				.build());
		
		redirectStrategy.sendRedirect(request, response, "/");
	}

}
