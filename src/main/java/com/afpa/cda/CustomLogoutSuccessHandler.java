package com.afpa.cda;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.afpa.cda.dao.UserDao;
import com.afpa.cda.entity.User;

public class CustomLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {


	@Autowired
	UserDao userDao;
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		System.err.println("bonjour");
		User u = userDao.findByName(authentication.getName()).get();
		userDao.save(
				User.builder()
				.id(u.getId())
				.name(u.getName())
				.password(u.getPassword())
				.role(u.getRole())
				.creation(u.getCreation())
				.active(false)
				.lastCo(u.getLastCo())
				.build());
		
		redirectStrategy.sendRedirect(request, response, "/");
	}
}	
