<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>${param.titre}</title>
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark w-100">
<div class="collapse navbar-collapse" id="navbarNav" style="color:white;">
<sec:authorize access="hasAnyRole('USER','ADMIN')" >
	<sec:authentication property="principal.username"/>
	</sec:authorize>
	</div>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav nav-tabs nav-fill w-150">
      <sec:authorize access="!isAuthenticated()">
      <li class="nav-item">
        <a class="nav-link ${param.navActive eq 'login'?'active':'' }" href="/login.html">login</a>
      </li>
      </sec:authorize>
      <sec:authorize access="isAuthenticated()">
      <li class="nav-item">
        <a class="nav-link " href="/logout">logout</a>
      </li>
      </sec:authorize>
 
    </ul>
  </div>
</nav>

